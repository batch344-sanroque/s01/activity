import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;

        Scanner myObj = new Scanner(System.in);

        System.out.println("First Name:");
        firstName = myObj.nextLine();
        System.out.println("Last Name:");
        lastName = myObj.nextLine();
        System.out.println("First Subject Grade:");
        firstSubject = myObj.nextInt();
        System.out.println("Second Subject Grade:");
        secondSubject = myObj.nextInt();
        System.out.println("Third Subject Grade:");
        thirdSubject = myObj.nextInt();

        double avgGrade = (firstSubject + secondSubject + thirdSubject) / 3;

        System.out.println("Good Day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + avgGrade);
        }
    }
